clone-os-bootable
=================

I use this script to create bootable clones of my (Linux) operating
systems. The clone can be fully encrypted (except the EFI system
partition, of course).

The fallback OS can be stored on a reasonably sized USB-key, external
hard drive, etc.

Once set up (see below), this script can be run as a scheduled task
(cron, etc.) to automatically update the backup regularly.

preconditions
-------------

**Warning:** As you can imagine, this is a quite complex process with as
much corner cases as there are possible configurations (disks,
boot loaders, fstabs, file systems, …). Use with caution, test with a
non-productive setup first, have backups, contribute fixes.

This script supports:

* Grub bootloader for either EFI or BIOS/lagacy
* a single root file system (including ``/boot``)
* a luks-encrypted root file system (including ``/boot``)

initial setup
-------------

If you are starting off, proceed as follows

#. partition your destination device

   * if using EFI: add a EFI system partition (ESP)

     * remember to set the partition type correctly

   * root file system

     * if not using EFI: remember to set the boot flag

#. if using EFI, format the ESP (FAT32)
#. optional: ``cryptsetup`` the partition for the root file system;
   attention: must be ``luks1`` format
#. format the partition for the root file system
#. read usage_ carefully
#. do the first run
#. verify the backup boots and contains what you expect it to contain
#. schedule regular backups

usage
-----

::
