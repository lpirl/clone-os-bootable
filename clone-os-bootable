#!/usr/bin/env python3

"""
This scripts gives its best to create a bootalbe backup of the current
file system root (/).

The backup **MUST reside on a separate device (partition is not
enough)** since we have to update Grub.
"""

import sys
import argparse
from logging import DEBUG, INFO, ERROR, getLogger, info, debug, error
from os import rmdir, sync, umask, mkdir
from os.path import basename, join, sep as path_sep, exists, isdir
from subprocess import (run as subprocess_run, DEVNULL, PIPE,
                        CalledProcessError)
from tempfile import mkdtemp
from re import sub
from fileinput import FileInput
from shlex import split as shsplit
from itertools import chain



RSYNC_OPTS = (
  '--archive',
  '--relative',
  '--verbose',
  '--delete-during',
  '--one-file-system',
  '--inplace',
  '--exclude=/proc/*',
  '--exclude=/dev/*',
  '--exclude=/tmp/*',
  '--exclude=/sys/*',
  '--exclude=/run/*',
  '--exclude=/var/cache/*',
  '--exclude=/var/lock/*',
  '--exclude=/var/log/*',
  '--exclude=/var/mail/*',
  '--exclude=/var/spool/*',
)

PARTITION_NUMBER_PATTERN = r'[0-9]+$'
CRYPTSETUP_PATH_TEMPLATE = '/dev/mapper/%s'
CRYPTSETUP_NAME = 'cryptroot'
DEVICE_BY_UUID_TEMPLATE = '/dev/disk/by-uuid/%s'

TEMPDIR_PREFIX = basename(sys.argv[0]) + '_'



class Cleaner:
  '''
  Instances can take care of running cleanup jobs.

  Used to collect jobs which have to be run upon program termination.

  It is really just a job queue (more precisely, a stack).
  '''

  def __init__(self):
    '''
    Initializes instance variables.
    '''
    self._jobs = []


  def add_job(self, func, *args, **kwargs):
    '''
    Adds :param:`func` with :param:`args` and :param:`kwargs` to list of
    cleanup jobs.
    '''
    self._jobs.append((func, args, kwargs))


  def do_all_jobs(self):
    '''
    Runs all queued cleanup jobs, in reverse order.
    '''
    while self._jobs:
      self.do_one_job()


  def do_one_job(self):
    '''
    Runs the most recently queued cleanup job.
    '''
    # in reverse order:
    func, args, kwargs = self._jobs.pop()
    debug('cleanup: func=%s.%s, args=%r, kwargs=%r', func.__module__,
          func.__name__, args, kwargs)
    func(*args, **kwargs)



class FileSystem:
  '''
  Provides all required information about a file system (see assert
  statements at the end of __init__) and does everything to get them.
  '''


  CHROOT_BINDS = (
    '/proc',
    '/sys',
    '/dev',
  )
  '''
  A list of paths that have to be mounted to chroot environments.
  '''


  @staticmethod
  def get_partition_by_uuid(uuid):
    '''
    Returns the path to the device that represents the partition which
    contains the file system with :param:`uuid`.
    '''
    lsblk_result = run(('blkid', '-U', uuid),
                       stdout=PIPE)
    partition = lsblk_result.stdout.decode().strip()
    assert partition.startswith('/dev/')
    return partition


  @staticmethod
  def get_device_by_mount_point(mount_point):
    '''
    Returns the path to the device that represents the partition which
    contains the file system which is mounted to :param:`mountpoint`.
    '''
    df_result = run(
      (
        ' | '.join((
          'df --no-sync --output=source "%s"' % mount_point,
          'grep /dev/'
        )),
      ),
      shell=True, stdout=PIPE
    )
    device = df_result.stdout.decode().strip()
    assert device.startswith('/dev/')
    return device


  @staticmethod
  def get_uuid_by_device(partition_device_path):
    '''
    Returns UUID of the file system that resides on the partition which
    is specified by the path to the device
    :param:`partition_device_path`.
    '''
    lsblk_result = run(
      ('blkid', '-o', 'value', '-s', 'UUID', partition_device_path),
      stdout=PIPE
    )
    uuid = lsblk_result.stdout.decode().strip()
    assert len(uuid) in [9, 36]
    return uuid


  def __init__(self, cleaner, mountpoint=None, uuid=None,
               mount_options=None, password=None, password_file=None):
    '''
    An abstract representation of a mounted file system; includes some
    helpers to work with it.

    If the file systems resides within a luks encrypted partition, the
    luks device will be opened first.

    If file system is specified by :param:`uuid`, the file system will
    be mounted to a temporary location.
    '''

    if (mountpoint and uuid) or (not mountpoint and not uuid):
      RuntimeError('Specify either ``mountpoint`` or ``uuid``.')

    self.chroot_prepared = False
    self.cleaner = cleaner
    self.mount_options = mount_options

    # set device path _and_ uuid from mountpoint _xor_ uuid:
    if mountpoint:
      self.mountpoint = mountpoint
      debug('loading file system: %s', self.mountpoint)
      self.partition_device_path = self.get_device_by_mount_point(
        self.mountpoint
      )
      self.partition_uuid = self.get_uuid_by_device(
        self.partition_device_path
      )
    elif uuid:
      self.partition_uuid = uuid
      debug('loading file system information for UUID: %s', uuid)
      self.partition_device_path = self.get_partition_by_uuid(uuid)

    debug('device is: %s', self.partition_device_path)
    debug('UUID is: %s', self.partition_uuid)

    if self.is_luks():
      debug('partition is LUKS encrypted')
      self._init_password_file(password, password_file)
      self.filesystem_device_path = (CRYPTSETUP_PATH_TEMPLATE %
                                     self.partition_uuid)
      self._luks_open()
      debug('actual device for file system is: %s',
            self.filesystem_device_path)
      self.filesystem_uuid = self.get_uuid_by_device(
        self.filesystem_device_path
      )
      debug('actual file system UUID is: %s', self.filesystem_uuid)
    else:
      debug('partition is not LUKS encrypted')
      if password or password_file:
        error('You specified a LUKS password but the target does '
              'not seem to be a LUKS encrypted partition!?')
        sys.exit(2)
      self.filesystem_device_path = self.partition_device_path
      self.filesystem_uuid = self.partition_uuid

    if not mountpoint:
      self._mount()

    # at the end of the day, this is what we need in any case:
    #   (at least to generate attributes we need in any case)
    #   (assuming 'context aware' asserts happened earlier)
    assert self.partition_device_path
    assert self.partition_uuid
    assert self.filesystem_device_path
    assert self.filesystem_uuid
    assert self.mountpoint


  def _init_password_file(self, password, password_file):
    '''
    Ensures password to resides in a file.

    If the password is given as string, it will be written to a
    temporary file. If password file is given, nothing needs to be done.

    Both, a password as string and a password file, cannot be given
    (because it is ambiguous).
    '''
    if password_file:
      self.password_file = password_file
      return
    if not password:
      error('Please specify either a password or a password file!')
      sys.exit(4)

    old_umask = umask(0o177)
    self.password_file = make_tempfile(self.cleaner, 'password')
    umask(old_umask)

    debug('writing password temporarily to "%s"', self.password_file)
    with open(self.password_file, 'w') as password_filep:
      password_filep.write(password)


  def is_luks(self):
    '''
    Returns whether this file system is luks encrypted.
    '''
    result = run(('file', '-ELs', self.partition_device_path),
                 stdout=PIPE)
    return 'LUKS encrypted' in result.stdout.decode().strip()


  def _luks_open(self):
    '''
    Opens luks encrypted partition.
    '''
    debug('opening LUKS')
    try:
      run(('cryptsetup', 'open', '--key-file', self.password_file,
           self.partition_device_path, self.partition_uuid))
    except CalledProcessError as cryptsetup_exception:
      if cryptsetup_exception.returncode == 2:
        error('could not open LUKS device: wrong passphrase')
        sys.exit(3)
      raise cryptsetup_exception
    else:
      self.cleaner.add_job(
        run, ('cryptsetup', 'close', '--deferred', self.partition_uuid)
      )


  def _mount(self):
    '''
    Mounts this file system to a temporary location.
    '''
    debug('creating temporary directory to mount: %s', self.filesystem_uuid)
    self.mountpoint = make_tempdir(self.cleaner, self.filesystem_uuid)
    info('mounting "%s" to "%s"', self.filesystem_uuid, self.mountpoint)
    mount_args = ['mount']
    if self.mount_options:
      mount_args.append('-o')
      mount_args.append(','.join(self.mount_options))
    mount_args.append('UUID=%s' % self.filesystem_uuid)
    mount_args.append(self.mountpoint)
    run(mount_args)
    self.cleaner.add_job(run, ('umount', '--lazy', self.mountpoint))


  def path(self, *bits):
    '''
    Return the path that is specified by :param:`bits` relative to
    this file systems' mountpoint.

    Absolute paths in ``bits`` will be made relative.
    '''
    out_path = self.mountpoint
    for bit in bits:
      bit = bit.lstrip(path_sep)
      out_path = join(out_path, bit)
    return out_path


  @property
  def device_path(self):
    '''
    Returns the path to the device that represents the partition this
    file system resides on.
    '''
    device = sub(PARTITION_NUMBER_PATTERN, '', self.partition_device_path)
    assert device.startswith('/dev/')
    return device


  def prepare_chroot(self, src_fs):
    '''
    Prepare chroot for this file system, bind-mount ``CHROOT_BINDS``
    from :param:`src_fs`.
    '''
    assert not self.chroot_prepared
    debug('mounting %s to target root', self.CHROOT_BINDS)
    for bind in self.CHROOT_BINDS:
      run(('mount', '--bind', src_fs.path(bind), self.path(bind)))
      self.cleaner.add_job(run, ('umount', '--lazy', self.path(bind)))
    self.cleaner.add_job(setattr, self, 'chroot_prepared', False)
    self.chroot_prepared = True


  def run_chrooted(self, call_args, *args, **kwargs):
    '''
    Run a command in a chrooted environment of this file system.
    '''
    assert self.chroot_prepared
    run(chain(('chroot', self.mountpoint), call_args), *args, **kwargs)



def run(call_args, *args, **kwargs):
  '''
  Runs command specified by :param:`call_args`. :param:`args` and
  :param:`kwargs` are handed to :func:`subprocess.run`. Takes care of
  printing debug information (or not) according to log level.
  '''

  # enforce non-zero exit codes to raise an exception
  kwargs['check'] = True

  if getLogger().level <= DEBUG:
    kwargs.setdefault('stdout', sys.stdout)
    kwargs.setdefault('stderr', sys.stderr)
  else:
    kwargs.setdefault('stdout', DEVNULL)
    kwargs.setdefault('stderr', DEVNULL)

  # subprocess.Popen needs an indexable type as 1st arg
  # try to convert to tuple if required
  if not hasattr(call_args, '__getitem__'):
    call_args = tuple(call_args)

  debug('running "%s" with args: %s and kwargs: %s:',
        ' '.join(call_args), args, kwargs)
  return subprocess_run(call_args, *args, **kwargs)



def add_line_to_file(path, line):
  '''
  Appends :param:`line` to the file referenced bv :param:`path`.
  '''
  debug('adding to file "%s": %s', path, line)
  with open(path, 'a') as path_fp:
    path_fp.write('\n%s\n' % line)



def _process_lines_in_file(path, func):
  '''
  Applies :param:`func` to every line of the file referenced bv
  :param:`path`.
  '''
  debug('processing lines in file "%s"', path)
  with FileInput(path, inplace=True) as path_fp:
    for line in path_fp:
      sys.stdout.write(
        func(line)
      )



def replace_in_file(path, old, new):
  '''
  Substitutes :param:`old` against :param:`new`
  in every line of the file referenced bv :param:`path`.
  '''
  debug('replacing in file "%s": "%s" with "%s"', path, old, new)
  _process_lines_in_file(
    path, lambda line: line.replace(old, new)
  )



def sub_in_file(path, old, new):
  '''
  Substitutes :param:`old` against :param:`new` by regular expression
  in every line of the file referenced bv :param:`path`.
  '''
  debug('substituting in file "%s": "%s" with "%s"', path, old, new)
  _process_lines_in_file(
    path, lambda line: sub(old, new, line)
  )



def check_dependencies(dest_fs):
  '''
  Check existence of required packages.
  '''

  dpkg_result = run(('dpkg', '--get-selections'), stdout=PIPE)

  installed_packages = set(
    package for package, state in
    (line.split() for line in dpkg_result.stdout.decode().splitlines())
    if state == 'install'
  )

  if dest_fs.is_luks():
    for requirement in ['cryptsetup', 'cryptsetup-initramfs']:
      info('check if "%s" is installed', requirement)
      if requirement not in installed_packages:
        error('required package "%s" not installed', requirement)
        sys.exit(5)



def rsync(src_fs, dest_fs, additional_source_paths, includes=None,
          excludes=None, rsync_options=None):
  '''
  Copies contents from :param:`src_fs` to :param:`dest_fs`, taking
  care of includes, excludes and other options.
  '''

  run_args = ['rsync']
  run_args.extend(RSYNC_OPTS)

  debug('assembling rsync include and exclude args')
  for switch, args in (('--include', includes),
                       ('--exclude', excludes)):
    for arg in args or []:
      assert isinstance(arg, str) and arg != ''
      run_args.append(switch)
      run_args.append(arg)

  debug('assembling extra rsync args')
  for option in rsync_options or []:
    run_args.extend(shsplit(option))

  debug('assembling rsync source paths')
  run_args.append(src_fs.path('/'))
  for source in additional_source_paths or []:
    run_args.append(src_fs.path(source))

  assert dest_fs.mountpoint != '/', (
    'refuse to sync *to* root file system'
  )

  run_args.append(dest_fs.mountpoint)

  info('rsyncing to target file system')
  run(run_args)



def adapt_fstab(old_fs, new_fs, fix_in_fs=None):
  '''
  If :param:`fix_in_fs` is ``None``, fstab in :param:`dest_fs` will be
  edited.
  '''
  fix_in_fs = fix_in_fs or new_fs
  info('update fstab in target file system')
  replace_in_file(
    fix_in_fs.path('etc/fstab'),
    old_fs.filesystem_device_path,
    new_fs.filesystem_device_path
  )
  replace_in_file(
    fix_in_fs.path('etc/fstab'),
    old_fs.filesystem_uuid,
    new_fs.filesystem_uuid
  )



def adapt_grub_config(dest_fs):
  '''
  Cleans up target grub configuration; in particular, it disables
  resuming (hibernate) from a swap partition.
  '''
  info('update Grub configuration in target file system')
  debug('deleting any: "resume=…" in grub configuration')
  sub_in_file(dest_fs.path('etc/default/grub'),
              r'resume=[^"\'\t\n ]+', '')



def make_luks_root_bootable(dest_fs):
  '''
  Applies configurations to enable booting from a luks-encrypted root
  file system (i.e., add cryptsetup compatibility to initramfs and
  grub).
  '''

  if not dest_fs.is_luks():
    return

  debug('enabling encrypted disks in config of target Grub')
  # makes Grub decrypt block device (to access /boot)
  add_line_to_file(
    dest_fs.path('/etc/default/grub'),
    'GRUB_ENABLE_CRYPTODISK=y'
  )

  debug('adding support for cryptsetup in target initramfs')
  # soon deprecated (can be removed then)
  add_line_to_file(
    dest_fs.path('/etc/cryptsetup-initramfs/conf-hook'),
    'CRYPTSETUP=y'
    )
  dest_fs.run_chrooted(('update-initramfs', '-u'))

  debug('enabling cryptroot in kernel command line')
  # makes the scripts provided by ``cryptsetup-initramfs`` decrypt the
  # root device (to access / and hand over to systemd)
  replace_in_file(
    dest_fs.path('/etc/default/grub'),
    'GRUB_CMDLINE_LINUX="',
    'GRUB_CMDLINE_LINUX="cryptopts=source=%s,luks ' % (
      DEVICE_BY_UUID_TEMPLATE % dest_fs.partition_uuid,
    )
  )



TEMPDIR_ROOT = None # to be set by ``make_tempdir_root``
def make_tempdir_root(cleaner):
  '''
  Creates root directory for temporary files and directories,
  schedules its removal after program termination.
  '''
  global TEMPDIR_ROOT
  assert TEMPDIR_ROOT is None
  TEMPDIR_ROOT = mkdtemp(prefix=TEMPDIR_PREFIX)
  cleaner.add_job(rmdir, TEMPDIR_ROOT)



def make_tempdir(cleaner, name):
  '''
  Creates a temporary directory, returns its name,
  schedules its removal after program termination.
  '''
  dirname = join(TEMPDIR_ROOT, name)
  assert not isdir(dirname)
  mkdir(dirname)
  cleaner.add_job(rmdir, dirname)
  return dirname



def make_tempfile(cleaner, name):
  '''
  Creates a temporary file, returns its name,
  schedules its removal after program termination.
  '''
  filename = join(TEMPDIR_ROOT, name)
  assert not exists(filename)
  open(filename, 'a').close()
  cleaner.add_job(rmdir, filename)
  return filename



def write_bootloader(cleaner, dest_fs, dest_esp_uuid, esp_mountpoint):
  '''
  Writes either MBR/legacy or EFI bootloader to target disk.
  '''

  info('(re)writing bootloader')

  grub_install_cmd = [
    'grub-install',
    '--recheck',
  ]

  if not dest_esp_uuid:
    debug('(re)writing legacy MBR bootloader')
    # no need for special treatment

  else:
    debug('(re)writing EFI bootloader')

    src_esp_fs = FileSystem(cleaner, mountpoint=esp_mountpoint)
    dest_esp_fs = FileSystem(cleaner, uuid=dest_esp_uuid)

    assert len(set((dest_fs.filesystem_uuid, src_esp_fs.filesystem_uuid,
                    dest_esp_fs.filesystem_uuid))) == 3, (
      'UUIDs of {source, target} X {ESP, root file system} must be '
      'unique but got duplicates.'
    )

    info('replace entry for ESP partition in fstab')
    adapt_fstab(src_esp_fs, dest_esp_fs, fix_in_fs=dest_fs)

    info(f'mount {esp_mountpoint} in target file system '
         '(for Grub update)')
    dest_fs.run_chrooted(('mount', esp_mountpoint))
    cleaner.add_job(dest_fs.run_chrooted, ('umount', esp_mountpoint))

    grub_install_cmd.extend((
      '--force-extra-removable',
      '--no-nvram',
    ))

  grub_install_cmd.append(dest_fs.device_path)
  dest_fs.run_chrooted(grub_install_cmd)



def get_cli_parser():
  '''
  Defines command line interface, returns corresponding parser.
  '''


  parser = argparse.ArgumentParser(
    description=__doc__
  )

  parser.add_argument('-d', '--debug', action='store_true', default=False,
                      help='turn on debug messages')
  parser.add_argument('-v', '--verbose', action='store_true', default=False,
                      help='turn on verbose messages')
  parser.add_argument('-q', '--quiet', action='store_true', default=False,
                      help='suppress everything except errors')
  parser.add_argument('-b', '--write-bootloader', action='store_true',
                      default=False,
                      help='(re)write to bootloader (ESP/MBR); it '
                           'should not be required to do this every '
                           'time but it won\'t hurt either '
                           '(except wearing the target medium)')
  parser.add_argument('-m', '--mount_option', action='append',
                      dest='mount_options',
                      help='options for mounting file system to backup to')
  parser.add_argument('-r', '--rsync_option', action='append',
                      dest='rsync_options',
                      help='options to pass to rsync for copying files')
  parser.add_argument('-s', '--source', action='append', dest='source_paths',
                      help=('source paths to include other than (the ' +
                            'file system mount on) /'))
  parser.add_argument('-i', '--include', action='append', dest='includes',
                      help='paths to include in backup (see man 1 rsync)')
  parser.add_argument('-e', '--exclude', action='append', dest='excludes',
                      help='paths to exclude from backup (see man 1 rsync)')
  parser.add_argument('--esp-mountpoint', default='/boot/efi',
                      help=('path to where ESP system partition is '
                            'mounted to'))
  password_group = parser.add_mutually_exclusive_group()
  password_group.add_argument('-p', '--password',
                              help=('password for decryption if the '
                                    'UUID points to a LUKS encrypted ' +
                                    'partition'))
  password_group.add_argument('-f', '--password-file',
                              help=('file containing a LUKS password ' +
                                    'as first line (see -p)'))
  parser.add_argument('dest_uuid',
                      help='UUID of the file system to back up to')
  parser.add_argument('dest_esp_uuid', nargs='?',
                      help=('UUID of the ESP file system to back up to'))

  return parser



def setup_logging(log_quiet, log_verbose, log_debug):
  '''
  Configures the logger according to :param:`cli_args`.
  '''

  logger = getLogger()
  logger.name = ''
  if log_quiet:
    logger.setLevel(ERROR)
  if log_verbose:
    logger.setLevel(INFO)
  if log_debug:
    logger.setLevel(DEBUG)
  debug('logging set up')



def cleaned_main(cleaner):
  '''
  Main, top level coordination of the program.
  '''

  cli_args = get_cli_parser().parse_args()

  setup_logging(cli_args.quiet, cli_args.verbose, cli_args.debug)

  # last job (i.e. first job submitted) is to sync disks
  cleaner.add_job(sync)

  make_tempdir_root(cleaner)

  dest_fs = FileSystem(cleaner, uuid=cli_args.dest_uuid,
                       password=cli_args.password,
                       password_file=cli_args.password_file,
                       mount_options=cli_args.mount_options)

  check_dependencies(dest_fs)

  src_fs = FileSystem(cleaner, mountpoint='/')

  rsync(src_fs, dest_fs, cli_args.source_paths, cli_args.includes,
        cli_args.excludes, cli_args.rsync_options)

  dest_fs.prepare_chroot(src_fs)

  adapt_fstab(src_fs, dest_fs)

  adapt_grub_config(dest_fs)

  make_luks_root_bootable(dest_fs)

  info('update Grub configuration in target file system')
  dest_fs.run_chrooted(('update-grub',))

  if cli_args.write_bootloader:
    write_bootloader(cleaner, dest_fs, cli_args.dest_esp_uuid,
                     cli_args.esp_mountpoint)

  if cli_args.debug:
    global TEMPDIR_ROOT
    input('You can now investigate the mounted file systems in '
          f'{TEMPDIR_ROOT} (chroot should work as well).\n\n'
          f'Press press return to clean up {TEMPDIR_ROOT} '
          'and exit the program.')



def main():
  '''
  Wraps actual main procedure :func:`cleaned_main`, catches uncaught
  exceptions and, in any case, runs cleanup jobs.
  '''
  cleaner = Cleaner()
  exit_code = 0
  try:
    cleaned_main(cleaner)
  except KeyboardInterrupt:
    error('received keyboard interrupt')
    exit_code = 130
  except Exception as exception:
    error('abnormal termination (see error at end of output)')
    exit_code = 1
    raise exception
  finally:
    info('running cleanup jobs')
    cleaner.do_all_jobs()

  if exit_code == 0:
    info('success - please verify your backup (!)')

  sys.exit(exit_code)



if __name__ == '__main__':
  main()
