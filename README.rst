
clone-os-bootable
=================

I use this script to create bootable clones of my (Linux) operating
systems. The clone can be fully encrypted (except the EFI system
partition, of course).

The fallback OS can be stored on a reasonably sized USB-key, external
hard drive, etc.

Once set up (see below), this script can be run as a scheduled task
(cron, etc.) to automatically update the backup regularly.

preconditions
-------------

**Warning:** As you can imagine, this is a quite complex process with as
much corner cases as there are possible configurations (disks,
boot loaders, fstabs, file systems, …). Use with caution, test with a
non-productive setup first, have backups, contribute fixes.

This script supports:

* Grub bootloader for either EFI or BIOS/lagacy
* a single root file system (including ``/boot``)
* a luks-encrypted root file system (including ``/boot``)

initial setup
-------------

If you are starting off, proceed as follows

#. partition your destination device

   * if using EFI: add a EFI system partition (ESP)

     * remember to set the partition type correctly

   * root file system

     * if not using EFI: remember to set the boot flag

#. if using EFI, format the ESP (FAT32)
#. optional: ``cryptsetup`` the partition for the root file system;
   attention: must be ``luks1`` format
#. format the partition for the root file system
#. read usage_ carefully
#. do the first run
#. verify the backup boots and contains what you expect it to contain
#. schedule regular backups

usage
-----

::

  usage: clone-os-bootable [-h] [-d] [-v] [-q] [-b] [-m MOUNT_OPTIONS]
                           [-r RSYNC_OPTIONS] [-s SOURCE_PATHS] [-i INCLUDES]
                           [-e EXCLUDES] [--esp-mountpoint ESP_MOUNTPOINT]
                           [-p PASSWORD | -f PASSWORD_FILE]
                           dest_uuid [dest_esp_uuid]
  
  This scripts gives its best to create a bootalbe backup of the current file
  system root (/). The backup **MUST reside on a separate device (partition is
  not enough)** since we have to update Grub.
  
  positional arguments:
    dest_uuid             UUID of the file system to back up to
    dest_esp_uuid         UUID of the ESP file system to back up to
  
  optional arguments:
    -h, --help            show this help message and exit
    -d, --debug           turn on debug messages
    -v, --verbose         turn on verbose messages
    -q, --quiet           suppress everything except errors
    -b, --write-bootloader
                          (re)write to bootloader (ESP/MBR); it should not be
                          required to do this every time but it won't hurt
                          either (except wearing the target medium)
    -m MOUNT_OPTIONS, --mount_option MOUNT_OPTIONS
                          options for mounting file system to backup to
    -r RSYNC_OPTIONS, --rsync_option RSYNC_OPTIONS
                          options to pass to rsync for copying files
    -s SOURCE_PATHS, --source SOURCE_PATHS
                          source paths to include other than (the file system
                          mount on) /
    -i INCLUDES, --include INCLUDES
                          paths to include in backup (see man 1 rsync)
    -e EXCLUDES, --exclude EXCLUDES
                          paths to exclude from backup (see man 1 rsync)
    --esp-mountpoint ESP_MOUNTPOINT
                          path to where ESP system partition is mounted to
    -p PASSWORD, --password PASSWORD
                          password for decryption if the UUID points to a LUKS
                          encrypted partition
    -f PASSWORD_FILE, --password-file PASSWORD_FILE
                          file containing a LUKS password as first line (see -p)
